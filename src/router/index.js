import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import InventoryHome from '@/components/inventory_module/InventoryHome'
import InventoryConfig from '@/components/inventory_module/config/Home'
import HomeInventory from '@/components/inventory_module/inventory/Home'
import Item from '@/components/inventory_module/config/item/Item'
import ItemCategory from '@/components/inventory_module/config/item_category/ItemCategory'
import ManageInventory from '@/components/inventory_module/inventory/manage_inventory/ManageInventory'
import ManualRecount from '@/components/inventory_module/inventory/manual_recount/ManualRecount'
import PriceList from '@/components/inventory_module/inventory/price_list/PriceList'
import ItemTransfer from '@/components/inventory_module/item_transfer/ItemTransfer'
import InventoryDashboard from '@/components/inventory_module/Dashboard'
//  Account
import AccountHome from '@/components/account_module/AccountHome'
import Account from '@/components/account_module/account/Account'
import Roles from '@/components/account_module/account/Roles'
// HR
import HrHome from '@/components/hr_module/HrHome'
import HomeEmployees from '@/components/hr_module/employees/Home'
import EmployeeList from '@/components/hr_module/employees/employee_list/EmployeeList'
import EmployeeContract from '@/components/hr_module/employees/employee_contract/EmployeeContract'
import EmployeeAttendance from '@/components/hr_module/employees/employee_attendance/EmployeeAttendance'
import HRConfig from '@/components/hr_module/config/Home'
import Benefit from '@/components/hr_module/config/benefit/Benefit'
import ContractType from '@/components/hr_module/config/contract_type/ContractType'
import Schedule from '@/components/hr_module/config/schedule/Schedule'
import Cutoff from '@/components/hr_module/config/cutoff/Cutoff'
import EmployeeSalary from '@/components/hr_module/employees/employee_salary/EmployeeSalary'
import EmployeeAdvanced from '@/components/hr_module/employees/employee_advanced/EmployeeAdvanced'
// Configuration
import ConfigurationHome from '@/components/configuration_module/ConfigurationHome.vue'
import City from '@/components/configuration_module/city/City'
import Company from '@/components/configuration_module/company/Company'
import Country from '@/components/configuration_module/country/Country'
import Day from '@/components/configuration_module/day/Day'
import GovernmentId from '@/components/configuration_module/government_id/GovernmentId'
import Language from '@/components/configuration_module/language/Language'
import Packing from '@/components/configuration_module/packing/Packing'
import Position from '@/components/configuration_module/position/Position'
import Province from '@/components/configuration_module/province/Province'
import Religion from '@/components/configuration_module/religion/Religion'
import Affiliation from '@/components/configuration_module/affiliation/Affiliation'
import AffiliationType from '@/components/configuration_module/affiliation_type/AffiliationType'
import Unit from '@/components/configuration_module/unit/Unit'
//  Accounts
import Profile from '@/components/profile'
//  TimeRecord
import TimeRecord from '@/components/TimeRecord'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/time_record',
      name: 'Time Record',
      component: TimeRecord
    },
    {
      path: '/inventory_module',
      name: 'Inventory Module',
      redirect: { name: 'Inventory Dashboard' },
      component: InventoryHome,
      children: [
        {
          path: 'management',
          name: 'Management',
          component: HomeInventory,
          redirect: { name: 'Manage Inventory' },
          children: [
            {
              path: 'manage_inventory',
              name: 'Manage Inventory',
              component: ManageInventory
            },
            {
              path: 'manual_recount',
              name: 'Manual Recount',
              component: ManualRecount
            },
            {
              path: 'price_list',
              name: 'Price List',
              component: PriceList
            }]
        },
        {
          path: 'transfer',
          name: 'Transfer',
          component: ItemTransfer
        },
        {
          path: 'dashboard',
          name: 'Inventory Dashboard',
          component: InventoryDashboard
        },
        {
          path: 'config',
          name: 'Inventory Configuration',
          component: InventoryConfig,
          redirect: { name: 'Item Configuration' },
          children: [
            {
              path: 'item',
              name: 'Item Configuration',
              component: Item
            },
            {
              path: 'item_category',
              name: 'Item Category Configuration',
              component: ItemCategory
            }
          ]
        }
      ]
    },
    {
      path: '/hr_module',
      name: 'HR Module',
      component: HrHome,
      redirect: { name: 'Employees' },
      children: [
        {
          path: 'employees',
          name: 'Employees',
          component: HomeEmployees,
          redirect: { name: 'Employee List' },
          children: [
            {
              path: 'employee_list',
              name: 'Employee List',
              component: EmployeeList
            },
            {
              path: 'employee_advanced',
              name: 'Employee List',
              component: EmployeeAdvanced
            },
            {
              path: 'employee_contract',
              name: 'Employee Contract Info',
              component: EmployeeContract
            },
            {
              path: 'employee_attendance',
              name: 'Attendance',
              component: EmployeeAttendance
            },
            {
              path: 'employee_salary',
              name: 'Generate Salary',
              component: EmployeeSalary
            }
          ]
        },
        {
          path: 'config',
          name: 'HR Configuration',
          component: HRConfig,
          redirect: { name: 'Benefit Configuration' },
          children: [
            {
              path: 'benefit',
              name: 'Benefit Configuration',
              component: Benefit
            },
            {
              path: 'contract_type',
              name: 'Contract Type Configuration',
              component: ContractType
            },
            {
              path: 'schedule',
              name: 'Schedule Configuration',
              component: Schedule
            },
            {
              path: 'cutoff',
              name: 'Cutoff Configuration',
              component: Cutoff
            }
          ]
        }
      ]
    },
    {
      path: '/configuration_module',
      name: 'Configuration Module',
      component: ConfigurationHome,
      redirect: { name: 'Affiliation' },
      children: [
        {
          path: 'affiliation',
          name: 'Affiliation',
          component: Affiliation
        },
        {
          path: 'affiliation_type',
          name: 'Affiliation Type',
          component: AffiliationType
        },
        {
          path: 'city',
          name: 'City',
          component: City
        },
        {
          path: 'company',
          name: 'Company',
          component: Company
        },
        {
          path: 'country',
          name: 'Country',
          component: Country
        },
        {
          path: 'day',
          name: 'Day',
          component: Day
        },
        {
          path: 'government_id',
          name: 'Government Id',
          component: GovernmentId
        },
        {
          path: 'language',
          name: 'Language',
          component: Language
        },
        {
          path: 'packing',
          name: 'Packing',
          component: Packing
        },
        {
          path: 'position',
          name: 'Position',
          component: Position
        },
        {
          path: 'province',
          name: 'Province',
          component: Province
        },
        {
          path: 'religion',
          name: 'Religion',
          component: Religion
        },
        {
          path: 'unit',
          name: 'Unit',
          component: Unit
        }
      ]
    },
    {
      path: '/account_module',
      name: 'Account Module',
      redirect: { name: 'Account' },
      component: AccountHome,
      children: [
        {
          path: 'account',
          name: 'Account',
          component: Account
        },
        {
          path: 'roles',
          name: 'Roles',
          component: Roles
        }
      ]
    }
  ]
  // mode: 'history'
})
