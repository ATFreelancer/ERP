/**
 * Created by Elvin on 7/14/2017.
 */
import _ from 'lodash'
export default {
  data () {
    return {
      search: '',
      searchBy: 'name',
      sortBy: null,
      totalItems: 0,
      pagination: {},
      params: {
        orderBy: {
          condition: []
        },
        filter: {
          condition: [],
          afterGet: []
        },
        search: null,
        rowsPerPage: null,
        page: null,
        totalItems: null
      }
    }
  },
  watch: {
    search: {
      handler () {
        this.params.search = {
          by: this.searchBy,
          keyword: _.isEmpty(this.search) ? null : this.search
        }
        this.getAll(this.params)
      },
      deep: true
    },
    pagination: {
      handler (params) {
        this.params.descending = params.descending
        this.params.page = params.page
        this.params.rowsPerPage = params.rowsPerPage
        this.params.totalItems = params.totalItems
        this.params.orderBy = {
          condition: [
            {
              col: this.sortBy ? this.sortBy : params.sortBy,
              direction: params.descending ? 'desc' : 'asc'
            }
          ]
        }
        this.getAll(this.params)
      },
      deep: true
    }
  }
}
