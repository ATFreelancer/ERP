/**
 * Created by Elvin on 7/14/2017.
 */
import HandlingMessage from '../components/utilities/handling-message'
import ValidationMessage from '../components/utilities/validation-message'
import _ from 'lodash'
import config from '../store/config'

export default {
  data () {
    return {
      service: '',
      localhost: '',
      device: '',
      items: [],
      loading: true,
      dialog: false,
      delDialog: false,
      itemToDelete: null,
      mode: null,
      model: {},
      auth: null,
      saveDisable: false,
      monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    }
  },
  created () {
    if (window.sessionStorage.getItem('api_token')) {
      this.auth = this.$store.getters['accounts/accounts/auth']
    }
  },
  beforeCreate () {
    this.localhost = config.BASE_URL
    this.device = config.device
  },
  methods: {
    beforeGetAll () {},
    afterGetAll () {},
    beforeAdd () {},
    afterAdd () {},
    beforeEdit () {},
    afterEdit () {},
    beforeView () {},
    afterView () {},
    beforeSave () {},
    beforeSaveAfterValidation () {},
    beforeSaveSuccess () {},
    beforeSaveError () {},
    beforeUpdate () {},
    beforeUpdateSuccess () {},
    beforeUpdateError () {},
    beforeDel () {},
    beforeDelDisagree () {},
    beforeDelAgree () {},
    beforeDelAgreeSuccess () {},
    beforeDelAgreeError () {},
    beforeClear () {},
    resetModel () {},
    validations (val) {
      var validations = []
      if (!val.$dirty) return validations
      var keys = Object.keys(val.$params)
      for (var i = keys.length - 1; i >= 0; i--) {
        if (!val[keys[i]]) validations[i] = ValidationMessage[keys[i]]
      }
      return validations
    },
    getAll (params = this.params) {
      this.beforeGetAll()
      this.loading = true
      this.$store.dispatch(this.service + 'getAll', params).then((res) => {
        this.items = res.data.data
        this.totalItems = res.data.total
        this.loading = false
        this.afterGetAll()
      })
    },
    add (item = null) {
      this.beforeAdd(item)
      this.clear()
      this.dialog = true
      this.afterAdd(item)
    },
    edit (model) {
      this.beforeEdit()
      this.clear('Edit')
      this.model = _.cloneDeep(model)
      this.afterEdit()
      this.dialog = true
    },
    view (model) {
      this.beforeView()
      this.clear('View')
      this.model = model
      this.afterView()
      this.dialog = true
    },
    save (service = null, close = true) {
      var currentService = !service ? this.service : service
      this.beforeSave()
      this.$v.$touch()
      if (this.$v.$error) return
      this.beforeSaveAfterValidation()
      this.saveDisable = true
      this.$store.dispatch(currentService + 'post', this.model).then((res) => {
        var errmsg = HandlingMessage.saveSuccess
        var errtype = 'success'
        if (res.data.error) {
          errmsg = res.data.error
          errtype = 'error'
        } else {
          this.getAll()
          if (close) {
            this.dialog = false
          }
          this.beforeSaveSuccess()
        }
        this.$events.fire('alert-me', {type: errtype, message: errmsg})
      }, () => {
        this.beforeSaveError()
        this.$events.fire('alert-me', {type: 'error', message: HandlingMessage.saveError})
      })
    },
    update (model) {
      this.beforeUpdate(model)
      this.$v.$touch()
      if (this.$v.$error) return
      this.saveDisable = true
      this.$store.dispatch(this.service + 'put', this.model).then((res) => {
        var errmsg = HandlingMessage.saveSuccess
        var errtype = 'success'
        if (res.data.error) {
          errmsg = res.data.error
          errtype = 'error'
        } else {
          this.getAll()
          this.dialog = false
          this.beforeUpdateSuccess(model)
        }
        this.$events.fire('alert-me', {type: errtype, message: errmsg})
      }, () => {
        this.beforeUpdateError()
        this.$events.fire('alert-me', {type: 'error', message: HandlingMessage.updateError})
      })
    },
    del (del) {
      this.beforeDel()
      this.itemToDelete = del.id
      this.delDialog = true
    },
    delDisagree () {
      this.beforeDelDisagree()
      this.itemToDelete = null
      this.delDialog = false
    },
    delAgree (service, close = true) {
      var currentService = !service ? this.service : service
      this.beforeDelAgree()
      this.$store.dispatch(currentService + 'delete', this.itemToDelete).then((res) => {
        var errmsg = HandlingMessage.saveSuccess
        var errtype = 'success'
        if (res.data.error) {
          errmsg = res.data.error
          errtype = 'error'
        } else {
          this.getAll()
          if (close) {
            this.dialog = false
          }
          this.itemToDelete = null
          this.delDialog = false
          this.beforeDelAgreeSuccess()
        }
        this.$events.fire('alert-me', {type: errtype, message: errmsg})
      }, () => {
        this.beforeDelAgreeError()
        this.$events.fire('alert-me', {type: 'error', message: HandlingMessage.removeError})
      })
    },
    clear (mode = 'Add') {
      this.beforeClear()
      this.$v.$reset()
      this.mode = mode
      this.saveDisable = false
      this.resetModel()
    },
    timeDifference (min, max) {
      let a = this.$options.filters.strtotimemin(this.model.start_time)
      let b = this.$options.filters.strtotimemax(this.model.end_time)
      let hour = (b - a) / 60 / 60
      return hour >= 24 ? hour - 24 : hour
    },
    hourMintoMin (_hour, _min) {
      var hour = !_hour ? 0 : parseInt(_hour)
      var min = !_min ? 0 : parseInt(_min)
      var hourToMin = hour * 60
      var time = hourToMin + min
      return time / 60
    }
  }
}
