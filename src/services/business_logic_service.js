/**
 * Created by Elvin on 7/12/2017.
 */
import baseService from './base_service'
import axios from 'axios'
import _ from 'lodash'

export default{

  priceListInit () {
    return axios.get(baseService.getSource() + 'pricelist/init')
  },

  employeeListInit () {
    return axios.get(baseService.getSource() + 'employeelist/init')
  },

  employeeListRetrieve (id) {
    return axios.get(baseService.getSource() + 'employeelist/retrieve/' + id)
  },

  employeeListRecord (data) {
    let rawData = {
      data: _.isArray(data) ? data : [data]
    }
    return axios.post(baseService.getSource() + 'employeelist/record', rawData)
  },

  employeeClean (data) {
    let rawData = {
      data: _.isArray(data) ? data : [data]
    }
    return axios.delete(baseService.getSource() + 'employeelist/clean', rawData)
  }
}
