/**
 * Created by Elvin on 6/28/2017.
 */
import config from './service.config'
import axios from 'axios'
import _ from 'lodash'

export default{
  getSource () {
    return config.BASE_URL
  },

  getAll (url, params) {
    let newParams = {}
    if (params) {
      if (params.orderBy) {
        newParams.orderBy = params.orderBy
      }
      if (params.rowsPerPage) {
        newParams.per_page = params.rowsPerPage
      }
      if (params.filter) {
        newParams.filter = params.filter
      }
      if (params.search) {
        newParams.search = params.search
      }
      if (params.page) {
        newParams.page = params.page
      }
      if (params.joinBy) {
        newParams.joinBy = params.joinBy
      }
      if (params.include) {
        newParams.include = params.include
      }
      if (params.select) {
        newParams.select = params.select
      }
    }
    return axios.get(config.BASE_URL + url, { params: newParams })
  },

  getBy (url, key, value) {
    return axios.get(config.BASE_URL + url + '/' + key + '/' + value)
  },

  get (url, id) {
    return axios.get(config.BASE_URL + url + '/' + id)
  },

  post (url, data) {
    let rawData = {
      data: _.isArray(data) ? data : [data]
    }
    return axios.post(config.BASE_URL + url, rawData)
  },

  put (url, data) {
    let rawData = {
      data: _.isArray(data) ? data : [data]
    }
    return axios.put(config.BASE_URL + url, rawData)
  },

  delete (url, data) {
    let rawData = {
      data: _.isArray(data) ? data : [data]
    }
    return axios.delete(config.BASE_URL + url, { data: rawData })
  },

  deleteAll (url) {
    return axios.delete(config.BASE_URL + url + '/all')
  }
}

