/**
 * Created by Elvin on 6/28/2017.
 */
import baseService from '../base_service'

const url = 'item_transfer'
const urlPlural = 'item_transfers'
import axios from 'axios'
import _ from 'lodash'

export default{
  getSource () {
    return baseService.getSource() + urlPlural
  },

  getAll (params) {
    return baseService.getAll(urlPlural, params)
  },

  getBy (key, value) {
    return baseService.getBy(urlPlural, key, value)
  },

  get (id) {
    return baseService.get(url, id)
  },

  post (data) {
    return baseService.post(url, data)
  },

  put (data) {
    return baseService.put(url, data)
  },

  delete (data) {
    return baseService.delete(url, data)
  },

  transact (data) {
    let rawData = {
      data: _.isArray(data) ? data : [data]
    }
    return axios.post(baseService.getSource() + url + '/transact', rawData)
  }
}
