/**
 * Created by Elvin on 6/28/2017.
 */
import baseService from '../base_service'
import axios from 'axios'

const url = 'item'
const urlPlural = 'items'

export default{
  init () {
    return axios.get(baseService.getSource() + urlPlural + '/init')
  },

  getSource () {
    return baseService.getSource() + urlPlural
  },

  getAll (params) {
    return baseService.getAll(urlPlural, params)
  },

  getBy (key, value) {
    return baseService.getBy(urlPlural, key, value)
  },

  get (id) {
    return baseService.get(url, id)
  },

  post (data) {
    return baseService.post(url, data)
  },

  put (data) {
    return baseService.put(url, data)
  },

  delete (data) {
    return baseService.delete(url, data)
  }
}
