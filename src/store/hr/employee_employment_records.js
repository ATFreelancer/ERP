const url = 'employee_employment_record'
const urlPlural = 'employee_employment_records'
import config from '../config'

export default {
  state: {
    source: config.BASE_URL + urlPlural
  },
  getters: {
    getSource (state) {
      return state.source
    }
  },
  actions: {
    async getAll ({ dispatch, commit }, params) {
      return await dispatch('based/getAll', { url: urlPlural, params: params }, { root: true })
    },
    async getBy ({ dispatch, commit }, params) {
      return await dispatch('based/getBy', { url: urlPlural, key: params.key, value: params.value }, { root: true })
    },
    async get ({ dispatch, commit }, id) {
      return await dispatch('based/get', { url: url, id: id }, { root: true })
    },
    async post ({ dispatch, commit }, data) {
      return await dispatch('based/post', { url: url, data: data }, { root: true })
    },
    async put ({ dispatch, commit }, data) {
      return await dispatch('based/put', { url: url, data: data }, { root: true })
    },
    async delete ({ dispatch, commit }, data) {
      return await dispatch('based/delete', { url: url, data: data }, { root: true })
    },
    async deleteAll ({ dispatch, commit }) {
      return await dispatch('based/deleteAll', url, { root: true })
    }
  }
}
