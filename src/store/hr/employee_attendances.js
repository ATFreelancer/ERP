const url = 'employee_attendance'
const urlPlural = 'employee_attendances'
import config from '../config'
import axios from 'axios'

export default {
  state: {
    source: config.BASE_URL + urlPlural
  },
  getters: {
    getSource (state) {
      return state.source
    }
  },
  actions: {
    async getAll ({ dispatch, commit }, params) {
      return await dispatch('based/getAll', { url: urlPlural, params: params }, { root: true })
    },
    async getBy ({ dispatch, commit }, params) {
      return await dispatch('based/getBy', { url: urlPlural, key: params.key, value: params.value, params: params.params }, { root: true })
    },
    async get ({ dispatch, commit }, id) {
      return await dispatch('based/get', { url: url, id: id }, { root: true })
    },
    async post ({ dispatch, commit }, data) {
      return await dispatch('based/post', { url: url, data: data }, { root: true })
    },
    async record (context, data) {
      return await axios.post(config.BASE_URL + 'record', data)
    },
    async dailyreport (context, data) {
      return await axios.post(config.BASE_URL + 'dailyreport', data)
    },
    async records ({ dispatch, commit }) {
      return await axios.get(config.BASE_URL + 'records')
    },
    async put ({ dispatch, commit }, data) {
      return await dispatch('based/put', { url: url, data: data }, { root: true })
    },
    async delete ({ dispatch, commit }, data) {
      return await dispatch('based/delete', { url: url, data: data }, { root: true })
    },
    async deleteAll ({ dispatch, commit }) {
      return await dispatch('based/deleteAll', url, { root: true })
    }
  }
}
