/**
 * Created by Daniel on 11/16/2017.
 */

const url = 'salary'
const urlPlural = 'salaries'
import config from '../config'
import axios from 'axios'
import _ from 'lodash'

export default {
  state: {
    source: config.BASE_URL + urlPlural,
    salaries: []
  },
  getters: {
    getSource (state) {
      return state.source
    },
    getAll (state) {
      return _.sortBy(state.salaries, 'name')
    }
  },
  mutations: {
    init (state, salaries) {
      state.salaries = salaries
    }
  },
  actions: {
    async init ({ dispatch, commit }) {
      await dispatch('based/getAll', { url: urlPlural, params: null }, { root: true }).then((res) => {
        commit('init', res.data.data)
      })
    },
    async getAll ({ dispatch, commit }, params) {
      return await dispatch('based/getAll', { url: urlPlural, params: params }, { root: true })
    },
    async getBy ({ dispatch, commit }, params) {
      return await dispatch('based/getBy', { url: urlPlural, key: params.key, value: params.value, params: params.params }, { root: true })
    },
    async get ({ dispatch, commit }, id) {
      return await dispatch('based/get', { url: url, id: id }, { root: true })
    },
    async post ({ dispatch, commit }, data) {
      let post = await dispatch('based/post', { url: url, data: data }, { root: true })
      await dispatch('init')
      return post
    },
    async generate (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.post(config.BASE_URL + 'generate', rawData)
    },
    async put ({ dispatch, commit }, data) {
      let put = await dispatch('based/put', { url: url, data: data }, { root: true })
      await dispatch('init')
      return put
    },
    async delete ({ dispatch, commit }, data) {
      let del = await dispatch('based/delete', { url: url, data: data }, { root: true })
      await dispatch('init')
      return del
    },
    async deleteAll ({ dispatch, commit }) {
      let del = await dispatch('based/deleteAll', url, { root: true })
      await dispatch('init')
      return del
    }
  }
}
