import config from '../config'
import axios from 'axios'
import _ from 'lodash'

export default {
  actions: {
    async commonsInit () {
      return await axios.get(config.BASE_URL + 'commons/init')
    },

    async priceListInit () {
      return await axios.get(config.BASE_URL + 'pricelist/init')
    },

    async employeeListInit () {
      return await axios.get(config.BASE_URL + 'employeelist/init')
    },

    async employeeListRetrieve (context, id) {
      return await axios.get(config.BASE_URL + 'employeelist/retrieve/' + id)
    },

    async employeeListRecord (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.post(config.BASE_URL + 'employeelist/record', rawData)
    },

    async employeeClean (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.delete(config.BASE_URL + 'employeelist/clean', { data: rawData })
    },

    async employeeContractRecord (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.post(config.BASE_URL + 'employee_contract/record', rawData)
    },

    async employeeContractRetrieve (context, id) {
      return await axios.get(config.BASE_URL + 'employee_contract/retrieve/' + id)
    },

    async employeeContractClean (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.delete(config.BASE_URL + 'employee_contract/clean', { data: rawData })
    }
  }
}
