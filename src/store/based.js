/**
 * Created by Elvin on 7/19/2017.
 */
import axios from 'axios'
import config from './config'
import _ from 'lodash'

export default {
  actions: {
    getSource () {
      return config.BASE_URL
    },
    async getAll (context, data) {
      let newParams = {}
      if (data.params) {
        if (data.params.orderBy) {
          newParams.orderBy = data.params.orderBy
        }
        if (data.params.rowsPerPage) {
          newParams.per_page = data.params.rowsPerPage
        }
        if (data.params.filter) {
          newParams.filter = data.params.filter
        }
        if (data.params.search) {
          newParams.search = data.params.search
        }
        if (data.params.page) {
          newParams.page = data.params.page
        }
        if (data.params.joinBy) {
          newParams.joinBy = data.params.joinBy
        }
        if (data.params.include) {
          newParams.include = data.params.include
        }
        if (data.params.select) {
          newParams.select = data.params.select
        }
      }
      return await axios.get(config.BASE_URL + data.url, {params: newParams})
    },

    async getBy (context, data) {
      return await axios.get(config.BASE_URL + data.url + '/' + data.key + '/' + data.value, {params: data.params})
    },

    async get (context, data) {
      return await axios.get(config.BASE_URL + data.url + '/' + data.id)
    },

    async post (context, data) {
      let rawData = {
        data: _.isArray(data.data) ? data.data : [data.data]
      }
      return await axios.post(config.BASE_URL + data.url, rawData)
    },

    async put (context, data) {
      let rawData = {
        data: _.isArray(data.data) ? data.data : [data.data]
      }
      return await axios.put(config.BASE_URL + data.url, rawData)
    },

    async delete (context, data) {
      let rawData = {
        data: _.isArray(data.data) ? data.data : [data.data]
      }
      return await axios.delete(config.BASE_URL + data.url, {
        data: rawData
      })
    },

    async deleteAll (context, url) {
      return await axios.delete(config.BASE_URL + url + '/all')
    }
  }
}
