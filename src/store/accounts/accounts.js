/**
 * Created by Daniel on 11/03/2017.
 */

const url = 'account'
const urlPlural = 'accounts'
import config from '../config'
import axios from 'axios'
import _ from 'lodash'

export default {
  state: {
    source: config.BASE_URL + urlPlural,
    accounts: [],
    auth: {}
  },
  getters: {
    getSource (state) {
      return state.source
    },
    getAll (state) {
      return _.sortBy(state.accounts, 'name')
    },
    auth (state) {
      return state.auth
    }
  },
  mutations: {
    init (state, accounts) {
      state.accounts = accounts
    },
    auth (state, auth) {
      state.auth = auth
    }
  },
  actions: {
    async init ({ dispatch, commit }) {
      await dispatch('based/getAll', { url: urlPlural, params: null }, { root: true }).then((res) => {
        commit('init', res.data.data)
      })
    },
    async getAll ({ dispatch, commit }, params) {
      return await dispatch('based/getAll', { url: urlPlural, params: params }, { root: true })
    },
    async getBy ({ dispatch, commit }, params) {
      return await dispatch('based/getBy', { url: urlPlural, key: params.key, value: params.value }, { root: true })
    },
    async get ({ dispatch, commit }, id) {
      return await dispatch('based/get', { url: url, id: id }, { root: true })
    },
    async post ({ dispatch, commit }, data) {
      let post = await dispatch('based/post', { url: url, data: data }, { root: true })
      await dispatch('init')
      return post
    },
    async login (context, data) {
      return await axios.post(config.BASE_URL + 'login', data)
    },
    async authuser ({ dispatch, commit }) {
      return await axios.get(config.BASE_URL + 'authuser')
    },
    async changeaccount (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.put(config.BASE_URL + 'changeaccount', rawData)
    },
    async reset (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.put(config.BASE_URL + 'reset', rawData)
    },
    async disable (context, data) {
      let rawData = {
        data: _.isArray(data) ? data : [data]
      }
      return await axios.put(config.BASE_URL + 'disable', rawData)
    },
    async put ({ dispatch, commit }, data) {
      let put = await dispatch('based/put', { url: url, data: data }, { root: true })
      await dispatch('init')
      return put
    },
    async delete ({ dispatch, commit }, data) {
      let del = await dispatch('based/delete', { url: url, data: data }, { root: true })
      await dispatch('init')
      return del
    },
    async deleteAll ({ dispatch, commit }) {
      let del = await dispatch('based/deleteAll', url, { root: true })
      await dispatch('init')
      return del
    }
  }
}
