/**
 * Created by Daniel on 11/03/2017.
 */

const url = 'role'
const urlPlural = 'roles'
import config from '../config'
import _ from 'lodash'

export default {
  state: {
    source: config.BASE_URL + urlPlural,
    roles: []
  },
  getters: {
    getSource (state) {
      return state.source
    },
    getAll (state) {
      return _.sortBy(state.roles, 'name')
    }
  },
  mutations: {
    init (state, roles) {
      state.roles = roles
    }
  },
  actions: {
    async init ({ dispatch, commit }) {
      await dispatch('based/getAll', { url: urlPlural, params: null }, { root: true }).then((res) => {
        commit('init', res.data.data)
      })
    },
    async getAll ({ dispatch, commit }, params) {
      return await dispatch('based/getAll', { url: urlPlural, params: params }, { root: true })
    },
    async getBy ({ dispatch, commit }, params) {
      return await dispatch('based/getBy', { url: urlPlural, key: params.key, value: params.value }, { root: true })
    },
    async get ({ dispatch, commit }, id) {
      return await dispatch('based/get', { url: url, id: id }, { root: true })
    },
    async post ({ dispatch, commit }, data) {
      let post = await dispatch('based/post', { url: url, data: data }, { root: true })
      await dispatch('init')
      return post
    },
    async put ({ dispatch, commit }, data) {
      let put = await dispatch('based/put', { url: url, data: data }, { root: true })
      await dispatch('init')
      return put
    },
    async delete ({ dispatch, commit }, data) {
      let del = await dispatch('based/delete', { url: url, data: data }, { root: true })
      await dispatch('init')
      return del
    },
    async deleteAll ({ dispatch, commit }) {
      let del = await dispatch('based/deleteAll', url, { root: true })
      await dispatch('init')
      return del
    }
  }
}
