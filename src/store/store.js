import Vue from 'vue'
import Vuex from 'vuex'
import units from './commons/units'
import religions from './commons/religions'
import provinces from './commons/provinces'
import positions from './commons/positions'
import packings from './commons/packings'
import languages from './commons/languages'
import governmentIds from './commons/government_ids'
import days from './commons/days'
import countries from './commons/countries'
import contactPeople from './commons/contact_people'
import companies from './commons/companies'
import cities from './commons/cities'
import affiliations from './commons/affiliations'
import affiliationTypes from './commons/affiliation_types'
import inventories from './inventories/inventories'
import itemCategories from './inventories/item_categories'
import itemTransfers from './inventories/item_transfers'
import items from './inventories/items'
import benefits from './hr/benefits'
import contractTypes from './hr/contract_types'
import employeeAdvancedDetails from './hr/employee_advanced_details'
import employeeAdvanceds from './hr/employee_advanceds'
import employeeAttendances from './hr/employee_attendances'
import employeeBenefits from './hr/employee_benefits'
import employeeCharacterReferences from './hr/employee_character_references'
import employeeContracts from './hr/employee_contracts'
import employeeEducations from './hr/employee_educations'
import employeeEmploymentRecords from './hr/employee_employment_records'
import employeeGovernmentIds from './hr/employee_government_ids'
import employeeLanguageSpokens from './hr/employee_language_spokens'
import employeeRelations from './hr/employee_relations'
import employeeSchedules from './hr/employee_schedules'
import employeeSalaries from './hr/employee_salaries'
import employees from './hr/employees'
import schedules from './hr/schedules'
import businessLogic from './business_logics/business_logics'
import roles from './accounts/roles'
import accounts from './accounts/accounts'
import cutoffs from './hr/cutoffs'
import based from './based'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  modules: {
    based: {
      namespaced: true,
      modules: {
        based
      }
    },
    accounts: {
      namespaced: true,
      modules: {
        roles: {
          namespaced: true,
          modules: {
            roles
          }
        },
        accounts: {
          namespaced: true,
          modules: {
            accounts
          }
        }
      }
    },
    commons: {
      namespaced: true,
      modules: {
        units: {
          namespaced: true,
          modules: {
            units
          }
        },
        religions: {
          namespaced: true,
          modules: {
            religions
          }
        },
        provinces: {
          namespaced: true,
          modules: {
            provinces
          }
        },
        positions: {
          namespaced: true,
          modules: {
            positions
          }
        },
        packings: {
          namespaced: true,
          modules: {
            packings
          }
        },
        languages: {
          namespaced: true,
          modules: {
            languages
          }
        },
        government_ids: {
          namespaced: true,
          modules: {
            governmentIds
          }
        },
        days: {
          namespaced: true,
          modules: {
            days
          }
        },
        countries: {
          namespaced: true,
          modules: {
            countries
          }
        },
        contact_people: {
          namespaced: true,
          modules: {
            contactPeople
          }
        },
        companies: {
          namespaced: true,
          modules: {
            companies
          }
        },
        cities: {
          namespaced: true,
          modules: {
            cities
          }
        },
        affiliations: {
          namespaced: true,
          modules: {
            affiliations
          }
        },
        affiliation_types: {
          namespaced: true,
          modules: {
            affiliationTypes
          }
        }
      }
    },
    business_logics: {
      namespaced: true,
      modules: {
        businessLogic
      }
    },
    inventories: {
      namespaced: true,
      modules: {
        inventories: {
          namespaced: true,
          modules: {
            inventories
          }
        },
        item_categories: {
          namespaced: true,
          modules: {
            itemCategories
          }
        },
        item_transfers: {
          namespaced: true,
          modules: {
            itemTransfers
          }
        },
        items: {
          namespaced: true,
          modules: {
            items
          }
        }
      }
    },
    hr: {
      namespaced: true,
      modules: {
        benefits: {
          namespaced: true,
          modules: {
            benefits
          }
        },
        contract_types: {
          namespaced: true,
          modules: {
            contractTypes
          }
        },
        employee_advanced_details: {
          namespaced: true,
          modules: {
            employeeAdvancedDetails
          }
        },
        employee_advanceds: {
          namespaced: true,
          modules: {
            employeeAdvanceds
          }
        },
        employee_attendances: {
          namespaced: true,
          modules: {
            employeeAttendances
          }
        },
        employee_benefits: {
          namespaced: true,
          modules: {
            employeeBenefits
          }
        },
        employee_character_references: {
          namespaced: true,
          modules: {
            employeeCharacterReferences
          }
        },
        employee_contracts: {
          namespaced: true,
          modules: {
            employeeContracts
          }
        },
        employee_educations: {
          namespaced: true,
          modules: {
            employeeEducations
          }
        },
        employee_employment_records: {
          namespaced: true,
          modules: {
            employeeEmploymentRecords
          }
        },
        employee_government_ids: {
          namespaced: true,
          modules: {
            employeeGovernmentIds
          }
        },
        employee_language_spokens: {
          namespaced: true,
          modules: {
            employeeLanguageSpokens
          }
        },
        employee_relations: {
          namespaced: true,
          modules: {
            employeeRelations
          }
        },
        employee_schedules: {
          namespaced: true,
          modules: {
            employeeSchedules
          }
        },
        employees: {
          namespaced: true,
          modules: {
            employees
          }
        },
        schedules: {
          namespaced: true,
          modules: {
            schedules
          }
        },
        cutoffs: {
          namespaced: true,
          modules: {
            cutoffs
          }
        },
        employee_salaries: {
          namespaced: true,
          modules: {
            employeeSalaries
          }
        }
      }
    }
  }
})
