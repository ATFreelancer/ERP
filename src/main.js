import Vue from 'vue'
import Vuetify from 'vuetify'
import VueEvents from 'vue-events'
import App from './App'
import router from './router'
import lodash from 'lodash'
import Vuelidate from 'vuelidate'
import VueSession from 'vue-session'
import store from './store/store'
import moment from 'moment'

Vue.use(Vuetify)
Vue.use(lodash)
Vue.use(VueEvents)
Vue.use(Vuelidate)
Vue.use(VueSession)
Vue.use(moment)
Vue.config.productionTip = false

Vue.filter('twelvehour', function (value) {
  if (value) {
    return moment(value, 'h:mma').format('h:mma')
  }
  return null
})

Vue.filter('nullable', function (value) {
  if (value !== null) {
    return value
  }
  return ''
})

Vue.filter('datelong', function (value) {
  if (value) {
    return moment(value).format('MMM D, Y')
  }
})

Vue.filter('datetime', function (value) {
  if (value) {
    return moment(value).format('lll')
  }
})

Vue.filter('diffForHumans', function (value) {
  if (value) {
    return moment(value).fromNow()
  }
})

Vue.filter('longwithday', function (value) {
  return moment(value).format('(ddd) MMM D, Y')
})

Vue.filter('tomonth', function (value) {
  return moment(value).format('Y-MM')
})

Vue.filter('strtotimemin', function (value) {
  return moment.utc(value, 'h:mma').format('X')
})

Vue.filter('pesos', function (value) {
  return '₱' + String(parseFloat(value).toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
})

Vue.filter('decimal', function (value) {
  return String(parseFloat(value).toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
})

Vue.filter('comma', function (value) {
  return String(parseFloat(value).toFixed(0)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
})

Vue.filter('round2', function (value) {
  return parseFloat(value).toFixed(2)
})

Vue.filter('hourtocombo', function (value) {
  return Math.floor(value) + 'hr ' + String(((value % 1) * 60).toFixed(0)) + 'min'
})

Vue.filter('strtotimemax', function (value) {
  return moment.utc(value, 'h:mma').add(1, 'days').format('X')
})

Vue.filter('ordinal', function (i) {
  var j = i % 10
  var k = i % 100
  if (j === 1 && k !== 11) {
    return i + 'st'
  }
  if (j === 2 && k !== 12) {
    return i + 'nd'
  }
  if (j === 3 && k !== 13) {
    return i + 'rd'
  }
  return i + 'th'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
