/**
 * Created by Elvin on 7/3/2017.
 */
export default {
  getError: 'Failed to retrieve data!',
  saveSuccess: 'Data has been saved successfully!',
  saveError: 'Saving Error!',
  updateSuccess: 'Data has been updated!',
  updateError: 'Update Error!',
  removeSuccess: 'Data has been removed!',
  removeError: 'Removing Error!',
  /* Manage Inventory */
  noStores: 'No affiliation found. Register a Store first at [Configuration > Store].',
  undoSuccess: 'Undo successful',
  undoError: 'Undo unsuccessful',
  /* Item Transfer */
  transferSuccess: 'Item Transfer Success!',
  transferError: 'Item Transfer Error!'
}
