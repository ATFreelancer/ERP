/**
 * Created by Daniel on 10/25/2017.
 */
export default {
  required: 'This field is required',
  minLength: 'insufficient characters, add more characters',
  maxLength: 'too much characters, subtract characters',
  email: 'Email must be valid (ex. sample@sample.com)',
  numeric: 'must be numbers only!',
  requiredIf: 'this field is conditionally required',
  requiredUnless: 'input data is incorrect, clear first before data entry',
  sameAs: 'hello',
  between: 'value must be in range. Invalid value',
  // minDate: 'date value is below the expected minimum',
  // schedules
  uniqueDay: 'cannot repeat the same day',
  below4hours: 'schedule equivalent hour must not be less than 4 hours.',
  exceedsbreaktime: 'break deduction cannot be greater than break time',
  exceedsschedule: 'late meter cannot be greater than schedule',
  exceedsschedule2: 'break time cannot be greater than schedule',
  // contracts
  contractHiredate: 'invalid hiredate according to the employee\'s contract history',
  contractEnddate: 'invalid end of contract date according to the employee\'s contract history',
  // advance
  uniqueSelection: 'cannot accept duplicate items',
  exceedQuantity: 'quantity must not be more than existing stock'
}
