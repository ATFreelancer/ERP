/* Account */

INSERT INTO `users`(`username`, `password`, `first_name`, `middle_name`, `last_name`, `birthdate`, `gender`, `address`, `email`, `contact_number`, `description`, `remember_token`, `api_token`, `status`) VALUES ('0001','$2y$10$3NYepcloP2kHfUd3NhqeXOL5zhRIalfqon6u9Gq7sOzW3vDhWXCW6', 'Elvin', '', 'Teodoro', '','Male', '', 'elvinteodoro19@gmail.com', '', '', '' , '9Xu7AiVAykSoKlwlfkzUhsnPzlwLifnTF4Ao45wCkUDozhnf6DlKjcBaK6X7','0');

/* Country */

INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Philippines', 'PH', 'Filipino');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('United States of America', 'US', 'American');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Canada', 'CA', 'Canadian');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('China', 'CH', 'Chinese');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Japan', 'JP', 'Japanese');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Republic of Korea', 'KR', 'Koreans');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('France', 'FR', 'French');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Italy', 'IT', 'Italians');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Germany', 'DE', 'Germans');
INSERT INTO `countries`(`name`, `abbreviation`, `nationality`) VALUES ('Russian Federation', 'RU', 'Russians');

/* Province */

INSERT INTO `provinces`(`country_id`, `name`) VALUES (1, 'Metro Manila');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (1, 'Cebu');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (1, 'Quezon');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (2, 'Chicago');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (2, 'Los Angeles');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (2, 'San Francisco');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (1, 'Davao');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (1, 'Ilocos');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (3, 'Ontario');
INSERT INTO `provinces`(`country_id`, `name`) VALUES (3, 'Alberta');

/* City */

INSERT INTO `cities`(`province_id`, `name`) VALUES ('1', 'Manila');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('1', 'Caloocan');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('1', 'Pasay');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('2', 'Cebu');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('3', 'Quezon');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('4', 'Chicago');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('1', 'Pasig');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('1', 'Mandaluyong');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('9', 'Toronto');
INSERT INTO `cities`(`province_id`, `name`) VALUES ('1', 'Taguig');

/* Benefit */

INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Food Allowance', 1500, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Travel Allowance', 3000, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Rice Allowance', 1000, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Home Allowance', 1500, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Phone Allowance', 1500, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Weekly Allowance', 1000, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Monthly Allowance', 3000, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Sick Leave', 15, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Vacation Leave', 15, null);
INSERT INTO `benefits`(`name`, `value`, `description`) VALUES ('Emergency Leave', 10, null);

/* Contract Type */

INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Regular', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Irregular', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Provisional', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Contractual', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Weekly Contract', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Daily Contract', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('Monthly Contract', null);
INSERT INTO `contract_types`(`name`, `descriptions`) VALUES ('6 Months Contract', null);

/* Days */

INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Monday', 'Mon');
INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Tuesday', 'Tues');
INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Wednesday', 'Weds');
INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Thursday', 'Thur');
INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Friday', 'Fri');
INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Saturday', 'Sat');
INSERT INTO `days`(`name`, `abbreviation`) VALUES ('Sunday', 'Sun');

/* Schedule */
/*
INSERT INTO `schedules`(`name`, `start_time`, `end_time`, `descriptions`) VALUES ('Morning Shift', '04:00:00', '12:00:00', null)
INSERT INTO `schedules`(`name`, `start_time`, `end_time`, `descriptions`) VALUES ('Afternoon Shift', '12:00:00', '20:00:00', null)
INSERT INTO `schedules`(`name`, `start_time`, `end_time`, `descriptions`) VALUES ('Evening Shift', '20:00:00', '04:00:00', null)
*/
/* Government ID */

INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('Passport', 1, true);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('Passport', 2, true);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('NBI', 1, false);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('SSS', 1, false);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('TIN', 1, false);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('Company', 1, false);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('Passport', 3, true);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('Pag-IBIG', 1, false);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('NBI', 2, false);
INSERT INTO `government_ids`(`name`, `country_id`, `is_primary`) VALUES ('Passport', 4, true);

/* Language */

INSERT INTO `languages`(`name`, `description`) VALUES ('Tagalog', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('English', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Cebuano', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Ilocano', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Hiligaynon', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Waray-waray', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Kapampangan', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Bicolano', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Pangasinense', null);
INSERT INTO `languages`(`name`, `description`) VALUES ('Mandarin', null);

/* Religions */

INSERT INTO `religions`(`name`, `description`) VALUES ('Christianity ', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Born Again Christian', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Islam', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Iglesia Ni Kristo', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Atheist', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Hinduism', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Buddhism', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Chinese Traditional', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Judaism', null);
INSERT INTO `religions`(`name`, `description`) VALUES ('Primal-indigenous', null);

/* Positions */

INSERT INTO `positions`(`name`, `description`) VALUES ('President', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Vice President', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Chief Executive Officer', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Chief Operation Officer', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Chief Technical Officer', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Product Owner', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Developer', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('UI/UX Designer', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Quality Assurance', null);
INSERT INTO `positions`(`name`, `description`) VALUES ('Call center agent', null);

/* Units */
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Grams','g','grams',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Kilograms','kg','kilograms',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Milligrams','mg','milligrams',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Watts','w','watts',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Kilowatts','kw','kilowatts',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Liters','l','liters',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Kiloliters','kl','kiloliters',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Milliliters','ml','milliliters',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Meter','m','meter',Now(), Now());
INSERT INTO `units`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Kilometer','km','kilometer',Now(), Now());

/* Packings */

INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Packs','pack',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Plastics','plastic',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Bottles','bottle',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Boxes','box',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Cases','case',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Cardboards','cardboard',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Jars','jar',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Baskets','basket',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Cans','can',null,Now(), Now());
INSERT INTO `packings`(`name`, `abbreviation`, `description`, `created_at`, `updated_at`) VALUES ('Bags','bag',null,Now(), Now());

/* Affiliation Type */

INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Store','store',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Storage','storage',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Bakery','bakery',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Market','market',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Supermarket','supermarket',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Winery','winery',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Restaurant','restaurant',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Fastfood','fastfood',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Convinient Store','convinient store',Now(), Now());
INSERT INTO `affiliation_types`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Delivery Shop','delivery shop',Now(), Now());

/* Affiliation */

INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Dino Store', 1, 'Manila', '09171234567', 'dino_store@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Dino Storage', 2, 'Manila', '09171234567', 'dino_storage@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Dino Bakery', 3, 'Manila', '09171234567', 'dino_bakery@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Pritil Market', 4, 'Manila', '09171234567', 'pritil_market@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('SM Cubao Supermarket', 5, 'Quezon', '09171234567', 'sm_supermarket@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Cave Werdenberg', 6, 'Quezon', '09171234567', 'cave_werdenberg@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Ristorante delle Mitre', 7, 'Manila', '09171234567', 'ristorante_delle_mitre@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('Jolibee Vito Cruz', 8, 'Vito Cruz', '09171234567', 'dino_store@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('7-Eleven', 9, 'Vito Cruz', '09171234567', '711@gmail.com', null,Now(), Now());
INSERT INTO `affiliations`(`name`, `affiliation_type_id`, `address`, `contact_number`, `email_address`, `description`, `created_at`, `updated_at`) VALUES ('LBC', 10, 'Manila', '09171234567', 'lbc@gmail.com', null, Now(), Now());

/* Companies */

INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('San Miguel Corporation', 'Manila', 'sanmiguelcorp@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('ABS-CBN Corporation', 'Mandaluyong', 'abscbn@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('Procter & Gamble Philippines', 'Makati', 'procter&gamble@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('Google Philippines', 'Makati', 'googlephil@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('SM Investments', 'Manila', 'sminvestment@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('Coca-Cola FEMSA Philippines', 'Manila', 'cocacolaphil@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('Ayala Corp', 'Manila', 'ayalacorp@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('BDO Unibank', 'Manila', 'bdounibank@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('GMA Network Corporation', 'Pasig', 'gmanetwork@gmail.com', '09171234567', null, Now(), Now());
INSERT INTO `companies`(`name`, `address`, `email_address`, `contact_number`, `description`, `created_at`, `updated_at`) VALUES ('Emilio Aguinaldo Corporation', 'Manila', 'eac@gmail.com', '09171234567', null, Now(), Now());

/* Contact People */

INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (1, 'Karl', 'Bryan', 'Cabalfin', 'Sta Mesa', 'karl@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (2, 'Dino', 'Man', 'Alix', 'Vito Cruz', 'dino@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (3, 'Dyna', 'Girl', 'Alix', 'Vito Cruz', 'dyna@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (4, 'Daniel', 'Boy', 'Atienza', 'Manila', 'daniel@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (5, 'Johar', 'Payat', 'Carreon', 'Caloocan', 'johar@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (6, 'Edward', 'Lee', 'Ong', 'Pandacan', 'edward@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (7, 'Micx', 'Girl', 'Estrada', 'Manila', 'micx@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (8, 'Stephanie', 'Ann', 'Grace', 'Ilo-Ilo', 'stef@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (9, 'Uzumaki', 'Kagebunshin', 'Naruto', 'Konoha', 'naruto@gmail.com', '09171234567', Now(), Now());
INSERT INTO `contact_people`(`company_id`, `first_name`, `middle_name`, `last_name`, `address`, `email_address`, `contact_number`, `created_at`, `updated_at`) VALUES (10, 'Luffy', 'D', 'Monkey', 'Sea', 'onepiece@gmail.com', '09171234567', Now(), Now());

/* Item Categories */

INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Snacks','snacks', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Drinks','drinks', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Meats','meats', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Vegetables','vegetables', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Fruits','fruits', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Dairy Products','dairy products', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Canned Goods','canned goods', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Biscuits','biscuits', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Tools','tools', Now(), Now());
INSERT INTO `item_categories`(`name`, `description`, `created_at`, `updated_at`) VALUES ('Medicines','medicines', Now(), Now());

/* Items */

INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Chippy', 1, 1, 10, 1, 10, 10, 10, 10, 10, 10, 1, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Juice', 2, 2, 20, 2, 20, 20, 20, 20, 20, 20, 2, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Pork Chop', 3, 3, 30, 3, 30, 30, 30, 30, 30, 30, 3, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Carrots', 4, 4, 40, 4, 40, 40, 40, 40, 40, 40, 4, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Apple', 5, 5, 50, 5, 50, 50, 50, 50, 50, 50, 5, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Milks', 6, 6, 60, 6, 60, 60, 60, 60, 60, 60, 6, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Vienna Sausage', 7, 7, 70, 7, 70, 70, 70, 70, 70, 70, 7, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Oreos', 8, 8, 80, 8, 80, 80, 80, 80, 80, 80, 8, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Hammer', 9, 9, 90, 9, 90, 90, 90, 90, 90, 90, 9, null, Now(), Now());
INSERT INTO `items`(`name`, `company_id`, `unit_id`, `unit_value`, `packing_id`, `packing_value`, `unit_price`, `net_price`, `retail_price`, `wholesale_per_box`, `wholesale_per_piece`, `item_category_id`, `description`, `created_at`, `updated_at`) VALUES ('Neozep', 10, 10, 100, 10, 100, 100, 100, 100, 100, 100, 10, null, Now(), Now());

/* Inventories */

INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (1,100,1, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (2,100,1, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (3,100,1, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (4,100,1, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (5,100,1, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (2,100,2, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (3,100,3, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (4,100,4, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (5,100,5, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (6,100,6, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (7,100,7, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (8,100,8, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (9,100,9, Now(), Now());
INSERT INTO `inventories`(`item_id`, `quantity`, `affiliation_id`, `created_at`, `updated_at`) VALUES (10,100,10, Now(), Now());




